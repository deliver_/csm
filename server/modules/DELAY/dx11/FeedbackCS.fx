Texture2D texCOL <string uiname="Color Buffer";>;
SamplerState s0 <bool visible=false;string uiname="Sampler";> {Filter=MIN_MAG_MIP_LINEAR;AddressU=CLAMP;AddressV=CLAMP;};

int2 Reso=512;
bool Reset <bool bang=true;> =0;
float4 Fade <bool color=true;String uiname="Fade Color";> =.5;
RWStructuredBuffer<float4> Output : BACKBUFFER;


[numthreads(64, 1, 1)]
void CSFeedback( uint3 DTid : SV_DispatchThreadID )
{
	float2 uv=(float2(DTid.x%Reso.x,DTid.x/Reso.x)+0.5)/Reso;
	float3 c=Output[DTid.x].xyz;
    float4 cc = texCOL.SampleLevel(s0,uv,0);
	Output[DTid.x].xyz=lerp(lerp(cc.xyz,c,Fade.rgb), cc.xyz, cc.a);

	if(Reset)Output[DTid.x]=float4(texCOL.SampleLevel(s0,uv,0).xyz,1);
}



technique11 Feedback
{
	pass P0
	{
		SetComputeShader( CompileShader( cs_4_0, CSFeedback() ) );
	}
}
