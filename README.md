# Workshop CSM #

Dates of workshop: 08.04 - 12.04.2019

## to do ##

* get cracked version Windows10
* audio reactive pong & interactive hair
* short video = what is media art
* send testvideo to trainstation
* 3D Modell of Trainstation
* install teamviewer, fixed ip, vvvv, repo, clean desktop, +
* A4 paper
* tripot for kinect

### Screen infos ###
* sunset - 24:00 
* 04:00 - sunrise
* 50min loop
* 10.04 send test file 15:00 Uhr to trainstation
* send them one file in total

### Links ###
* https://www.random-international.com/swarm-study-ix
* https://www.schulmodell.eu/11-ueber-csm/wir-ueber-uns/2664-kunst-%E2%80%93-und-kulturprojektwoche-am-csm-08-04-12-04-2019.html
* https://www.bauhaus100.de/
* https://www.youtube.com/watch?v=FmThAic44GI
* http://michaelkipp.de/interaction/vvvv.html
#### PDF ####
* https://vvvv.org/sites/all/modules/general/pubdlcnt/pubdlcnt.php?file=https://vvvv.org/sites/default/files/uploads/vvvv_basic_0.9.pdf&nid=201283
* https://vvvv.org/sites/all/modules/general/pubdlcnt/pubdlcnt.php?file=https://vvvv.org/sites/default/files/uploads/dontpanic_45beta25_print_1.pdf&nid=56604

#### Video Tutorial ####

* https://www.youtube.com/user/v4tutorial/videos
* https://vimeo.com/59383288

#### Example patches ####
* https://github.com/PrototypingInterfaces/PrototypingInterfaces_patchcollection
* https://vvvv.org/contribution/parasitic-design-a-vvvv-beginners-cookbook
* http://www.generative-gestaltung.de/1/code

### technical Resources ###
* Mutter (Studip machine) - Server
* Vater (Studio machine) - Client01
* Sister (Studio machine) - Client02
* Brother (Studio machine) - Client03
* iMac Digitale Class - Client04 ?
* Laptop Quadrature (Studio machine) - Client05 ?
* Samsung Notebook (Studio machine) - Client06 ?
* 4 x Laptops from school
* 5 x Monitors

* 1 x projectors (Studio), 1 x projector (School)

* 2 x Speaker (Studio) , 1 x Mixer (Studio)

* 2 x Switch, 10 x network wire, 10 meter USB extension, 5 x mouse & keyboard

* 1 x Kinect

* 1 x 1080ti Nvida Spare

### --------------------------------------------------- ###

### VVVV Modules for activation areas ####
* SPRAY (done)
* DULICATOR (done)
* ROTATE (done)
* SCALER 
* ROPE (done)
* SLINGSHOT 
* SCALE_TO_VELOCITY
* EDGE_COALITION (done)
* CONNECT_ALL (done)
* DELAUNY (done)
* GRAVITY
* SIMPLEX (done)
* TRIANGLE
* ATTRACTOR (done)
* OPTICAL_FLOW

### Posteffects ####
* Echo
* Delay
* Blending
* Wormhole
* Glitch
* 

### Video Link ###

* http://archive.aec.at/prix/showmode/23842/
* http://archive.aec.at/prix/showmode/20738/
* http://archive.aec.at/prix/showmode/26534/
* http://archive.aec.at/prix/showmode/25050/
* https://artcom.de/project/kinetische-skulptur/
Dispaly:
* http://simonweckert.com/ropescreen.html

Robot:
* https://vimeo.com/103476879
* https://www.youtube.com/watch?v=2u8E-4YVANU
* https://www.youtube.com/watch?v=HX6M4QunVmA
* https://vimeo.com/270872133

Kinect:
* https://vimeo.com/46045360

Meme:
* closing box

### --------------------------------------------------- ###
### Day_01 ###

* Welcome round, HeadsUp, Timeplan
* Introduction into MediaArt
* Presentation of own art
* What is Programming
* Introduction into VVVV

### Day_02 ###

* Followup introduction in VVVV
* Introduction into Kinect

### Day_03 ###

* Perendering of animation
* Evening Meeting at Trainstation for first view of content

### Day_04 ###

* fine tuneing of content
* final rendering of content

### Day_05 ###

* exhibition preperation
